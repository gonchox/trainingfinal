package com.trainingfinal.demo.service;

import com.trainingfinal.demo.dto.WarehouseDTO;
import com.trainingfinal.demo.dto.WarehouseTypeDTO;
import com.trainingfinal.demo.model.Warehouse;
import com.trainingfinal.demo.model.WarehouseType;
import com.trainingfinal.demo.repository.WarehouseRepository;
import com.trainingfinal.demo.repository.WarehouseTypeRepository;
import com.trainingfinal.demo.service.Impl.WarehouseServiceImpl;
import com.trainingfinal.demo.service.Impl.WarehouseTypeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WarehouseServiceTest {
    @Mock
    private WarehouseRepository warehouseRepository;
    @Mock
    private WarehouseTypeRepository warehouseTypeRepository;

    @InjectMocks
    private WarehouseServiceImpl warehouseService;

    @Test
    void getWarehousesTest() {
        WarehouseType warehouseType1 = new WarehouseType(1,"publico");
        WarehouseType warehouseType2 = new WarehouseType(2,"privado");
        Warehouse warehouse = new Warehouse(1,warehouseType1, "Almacen 1");
        Warehouse warehouse2 = new Warehouse(2, warehouseType2 ,"Almacen 2");
        List<Warehouse> expectedWarehouses = new ArrayList<>();
        expectedWarehouses.add(warehouse);
        expectedWarehouses.add(warehouse2);

        when(warehouseRepository.getWarehouses()).thenReturn(expectedWarehouses);

        List<Warehouse> actualWarehouses = warehouseRepository.getWarehouses();
        assertEquals(expectedWarehouses.size(), actualWarehouses.size());
        assertEquals(expectedWarehouses, actualWarehouses);
    }

    @Test
    void getWarehouseByIdTest() {
        int id = 1;
        WarehouseType warehouseType1 = new WarehouseType(1,"publico");
        Warehouse expectedWarehouse = new Warehouse(1, warehouseType1,"Almacen 1");

        when(warehouseRepository.getWarehouseById(id)).thenReturn(expectedWarehouse);

        Warehouse actualWarehouse = warehouseService.getWarehouseById(id);

        assertNotNull(actualWarehouse);
        assertEquals(actualWarehouse.getName(), actualWarehouse.getName());
    }

    @Test
    void createWarehouseTest() {
        WarehouseType warehouseType = new WarehouseType();
        warehouseType.setId(1);
        warehouseType.setName("publico");

        WarehouseDTO warehouseDTO = new WarehouseDTO();
        warehouseDTO.setName("Almacen 1");
        warehouseDTO.setIdWarehouseType(1);

        when(warehouseTypeRepository.getWarehouseTypeById(1)).thenReturn(warehouseType);

        Warehouse expectedWarehouse = new Warehouse();
        expectedWarehouse.setName("Almacen 1");
        expectedWarehouse.setWarehouseType(warehouseType);

        when(warehouseRepository.createWarehouse(any(Warehouse.class))).thenReturn(expectedWarehouse);

        Warehouse actualWarehouse = warehouseService.createWarehouse(warehouseDTO);

        assertNotNull(actualWarehouse);
        assertEquals(expectedWarehouse.getName(), actualWarehouse.getName());
        assertEquals(expectedWarehouse.getWarehouseType(), actualWarehouse.getWarehouseType());
    }
}

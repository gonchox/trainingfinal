package com.trainingfinal.demo.service;

import com.trainingfinal.demo.dto.ProductTypeDTO;
import com.trainingfinal.demo.dto.WarehouseTypeDTO;
import com.trainingfinal.demo.model.ProductType;
import com.trainingfinal.demo.model.WarehouseType;
import com.trainingfinal.demo.repository.ProductTypeRepository;
import com.trainingfinal.demo.repository.WarehouseTypeRepository;
import com.trainingfinal.demo.service.Impl.ProductTypeServiceImpl;
import com.trainingfinal.demo.service.Impl.WarehouseTypeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WarehouseTypeServiceTest {

    @Mock
    private WarehouseTypeRepository warehouseTypeRepository;

    @InjectMocks
    private WarehouseTypeServiceImpl warehouseTypeService;

    @Test
    void getWarehouseTypesTest() {
        WarehouseType warehouseType = new WarehouseType(1, "publico");
        WarehouseType warehouseType2 = new WarehouseType(2, "privado");
        List<WarehouseType> expectedWarehouseTypes = new ArrayList<>();
        expectedWarehouseTypes.add(warehouseType);
        expectedWarehouseTypes.add(warehouseType2);

        when(warehouseTypeRepository.getWarehouseTypes()).thenReturn(expectedWarehouseTypes);

        List<WarehouseType> actualWarehouseTypes = warehouseTypeService.getWarehouseTypes();
        assertEquals(expectedWarehouseTypes.size(), actualWarehouseTypes.size());
        assertEquals(expectedWarehouseTypes, actualWarehouseTypes);
    }

    @Test
    void getWarehouseTypeByIdTest() {
        int id = 1;
        WarehouseType expectedWarehouseType = new WarehouseType(1, "publico");

        when(warehouseTypeRepository.getWarehouseTypeById(id)).thenReturn(expectedWarehouseType);

        WarehouseType actualWarehouseType = warehouseTypeService.getWarehouseTypeById(id);

        assertNotNull(actualWarehouseType);
        assertEquals(actualWarehouseType.getName(), actualWarehouseType.getName());
    }

    @Test
    void createWarehouseTypeTest() {
        WarehouseTypeDTO warehouseTypeDto = new WarehouseTypeDTO();
        warehouseTypeDto.setName("publico");

        WarehouseType expectedWarehouseType = new WarehouseType();
        expectedWarehouseType.setName("publico");

        when(warehouseTypeRepository.createWarehouseType(any(WarehouseType.class))).thenReturn(expectedWarehouseType);

        WarehouseType actualWarehouseType = warehouseTypeService.createWarehouseType(warehouseTypeDto);

        assertNotNull(actualWarehouseType);
        assertEquals(actualWarehouseType.getName(), expectedWarehouseType.getName());
    }
}

package com.trainingfinal.demo.service;

import com.trainingfinal.demo.dto.ProductTypeDTO;
import com.trainingfinal.demo.model.ProductType;
import com.trainingfinal.demo.repository.ProductTypeRepository;
import com.trainingfinal.demo.service.Impl.ProductServiceImpl;
import com.trainingfinal.demo.service.Impl.ProductTypeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ProductTypeServiceTest {

    @Mock
    private ProductTypeRepository productTypeRepository;

    @InjectMocks
    private ProductTypeServiceImpl productTypeService;

    @Test
    void getProductTypesTest() {
        ProductType productType = new ProductType(1, "ropa");
        ProductType productType2 = new ProductType(2, "zapatillas");
        List<ProductType> expectedProductTypes = new ArrayList<>();
        expectedProductTypes.add(productType);
        expectedProductTypes.add(productType2);

        when(productTypeRepository.getProductTypes()).thenReturn(expectedProductTypes);

        List<ProductType> actualProductTypes = productTypeService.getProductTypes();
        assertEquals(expectedProductTypes.size(), actualProductTypes.size());
        assertEquals(expectedProductTypes, actualProductTypes);
    }

    @Test
    void getProductTypeByIdTest() {
        int id = 1;
        ProductType expectedProductType = new ProductType(1, "roap");

        when(productTypeRepository.getProductTypeById(id)).thenReturn(expectedProductType);

        ProductType actualProductType = productTypeService.getProductTypeById(id);

        assertNotNull(actualProductType);
        assertEquals(actualProductType.getName(), actualProductType.getName());
    }

    @Test
    void createProductTypeTest() {
        ProductTypeDTO productTypeDto = new ProductTypeDTO();
        productTypeDto.setName("ropa");

        ProductType expectedProductType = new ProductType();
        expectedProductType.setName("ropa");

        when(productTypeRepository.createProductType(any(ProductType.class))).thenReturn(expectedProductType);

        ProductType actualProductType = productTypeService.createProductType(productTypeDto);

        assertNotNull(actualProductType);
        assertEquals(actualProductType.getName(), expectedProductType.getName());
    }

}

package com.trainingfinal.demo.repository;

import com.trainingfinal.demo.model.Warehouse;
import com.trainingfinal.demo.model.WarehouseType;
import com.trainingfinal.demo.repository.Impl.WarehouseRepositoryImpl;
import com.trainingfinal.demo.repository.Impl.WarehouseTypeRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WarehouseRepositoryTest {
    @Mock
    private JdbcTemplate jdbcTemplate;

    @InjectMocks
    private WarehouseRepositoryImpl warehouseRepositoryImpl;

    @Test
    void getWarehousesTest() {
        WarehouseType warehouseType1 = new WarehouseType(1,"publico");
        WarehouseType warehouseType2 = new WarehouseType(2,"privado");

        Warehouse warehouse = new Warehouse(1,warehouseType1, "Almacen 1");
        Warehouse warehouse2 = new Warehouse(2,warehouseType2, "Almacen 2");
        List<Warehouse> expectedWarehouses = new ArrayList<>();
        expectedWarehouses.add(warehouse);
        expectedWarehouses.add(warehouse2);

        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(expectedWarehouses);

        List<Warehouse> actualWarehouses =warehouseRepositoryImpl.getWarehouses();
        assertEquals(expectedWarehouses.size(),actualWarehouses.size());
        assertEquals(expectedWarehouses, actualWarehouses);

    }


    @Test
    void getWarehouseIdTest() {
        //Expected
        int id = 1;
        WarehouseType warehouseType1 = new WarehouseType(1,"publico");
        Warehouse expectedWarehouse = new Warehouse(1,warehouseType1, "Almacen 1");

        when(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class),
                anyString())).thenReturn(expectedWarehouse);

        Warehouse actualWarehouse = warehouseRepositoryImpl.getWarehouseById(id);

        assertNotNull(actualWarehouse);
        assertEquals(actualWarehouse.getName(), expectedWarehouse.getName());

    }


    @Test
    void mapToWarehouseTest() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        //Expected
        WarehouseType warehouseType1 = new WarehouseType(1,"publico");
        Warehouse expectedWarehouse = new Warehouse(1,warehouseType1, "Almacen 1");

        when(resultSet.getInt("id")).thenReturn(1);
        when(resultSet.getString("name")).thenReturn("Almacen 1");

        Warehouse actualWarehouse = warehouseRepositoryImpl.mapToWarehouse(resultSet);

        assertNotNull(actualWarehouse);
        assertEquals(actualWarehouse.getId(), expectedWarehouse.getId());
        assertEquals(actualWarehouse.getName(), expectedWarehouse.getName());

    }
}

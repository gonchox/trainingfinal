package com.trainingfinal.demo.repository;

import com.trainingfinal.demo.model.ProductType;
import com.trainingfinal.demo.repository.Impl.ProductTypeRepositoryImpl;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;


@ExtendWith(MockitoExtension.class)
public class ProductTypeRepositoryTest {
    @Mock
    private JdbcTemplate jdbcTemplate;

    @InjectMocks
    private ProductTypeRepositoryImpl productTypeRepositoryImpl;

    @Test
    void getProductTypesTest() {
        //Expected
        ProductType productType = new ProductType(1, "liquidos");
        ProductType productType2 = new ProductType(2, "cereales");
        List<ProductType> expectedProductTypes = new ArrayList<>();
       expectedProductTypes.add(productType);
        expectedProductTypes.add(productType2);

        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(expectedProductTypes);

        List<ProductType> actualProductTypes = productTypeRepositoryImpl.getProductTypes();
        assertEquals(expectedProductTypes.size(),actualProductTypes.size());
        assertEquals(expectedProductTypes, actualProductTypes);

    }


    @Test
    void getProductTypeByIdTest() {
        //Expected
        int id = 1;
        ProductType expectedProductType = new ProductType(1, "liquidos");

        when(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class),
                anyString())).thenReturn(expectedProductType);

        ProductType actualProductType = productTypeRepositoryImpl.getProductTypeById(id);

        assertNotNull(actualProductType);
        assertEquals(actualProductType.getName(), expectedProductType.getName());

    }


    @Test
    void mapToProductTypeTest() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        //Expected
        ProductType expectedProductType = new ProductType(1, "liquidos");

        when(resultSet.getInt("id")).thenReturn(1);
        when(resultSet.getString("name")).thenReturn("liquidos");

        ProductType actualProductType = productTypeRepositoryImpl.mapToProductType(resultSet);

        assertNotNull(actualProductType);
        assertEquals(actualProductType.getId(), expectedProductType.getId());
        assertEquals(actualProductType.getName(), expectedProductType.getName());

    }

}

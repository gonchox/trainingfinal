package com.trainingfinal.demo.repository;

import com.trainingfinal.demo.model.WarehouseType;
import com.trainingfinal.demo.repository.Impl.WarehouseTypeRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WarehouseTypeRepositoryTest {

    @Mock
    private JdbcTemplate jdbcTemplate;

    @InjectMocks
    private WarehouseTypeRepositoryImpl warehouseTypeRepositoryImpl;

    @Test
    void getWarehouseTypesTest() {
        //Expected
        WarehouseType warehouseType = new WarehouseType(1, "publico");
        WarehouseType warehouseType2 = new WarehouseType(2, "privado");
        List<WarehouseType> expectedWarehouseTypes = new ArrayList<>();
        expectedWarehouseTypes.add(warehouseType);
        expectedWarehouseTypes.add(warehouseType2);

        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(expectedWarehouseTypes);

        List<WarehouseType> actualWarehouseTypes =warehouseTypeRepositoryImpl.getWarehouseTypes();
        assertEquals(expectedWarehouseTypes.size(),actualWarehouseTypes.size());
        assertEquals(expectedWarehouseTypes, actualWarehouseTypes);

    }


    @Test
    void getWarehouseTypeByIdTest() {
        //Expected
        int id = 1;
        WarehouseType expectedWarehouseType = new WarehouseType(1, "publico");

        when(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class),
                anyString())).thenReturn(expectedWarehouseType);

        WarehouseType actualWarehouseType = warehouseTypeRepositoryImpl.getWarehouseTypeById(id);

        assertNotNull(actualWarehouseType);
        assertEquals(actualWarehouseType.getName(), expectedWarehouseType.getName());

    }


    @Test
    void mapToWarehouseTypeTest() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        //Expected
        WarehouseType expectedWarehouseType = new WarehouseType(1, "publico");

        when(resultSet.getInt("id")).thenReturn(1);
        when(resultSet.getString("name")).thenReturn("publico");

        WarehouseType actualWarehouseType = warehouseTypeRepositoryImpl.mapToWarehouseType(resultSet);

        assertNotNull(actualWarehouseType);
        assertEquals(actualWarehouseType.getId(), expectedWarehouseType.getId());
        assertEquals(actualWarehouseType.getName(), expectedWarehouseType.getName());

    }
}

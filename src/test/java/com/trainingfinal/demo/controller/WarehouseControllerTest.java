package com.trainingfinal.demo.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trainingfinal.demo.dto.WarehouseDTO;
import com.trainingfinal.demo.dto.WarehouseTypeDTO;
import com.trainingfinal.demo.model.Warehouse;
import com.trainingfinal.demo.model.WarehouseType;
import com.trainingfinal.demo.service.WarehouseService;
import com.trainingfinal.demo.service.WarehouseTypeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(WarehouseController.class)
public class WarehouseControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WarehouseService warehouseService;

    @Test
    void getWarehousesTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        WarehouseType warehouseType1 = new WarehouseType(1, "publico");
        WarehouseType warehouseType2 = new WarehouseType(2, "privado");
        Warehouse warehouse = new Warehouse(1, warehouseType1,"Almacen 1");
        Warehouse warehouse2 = new Warehouse(2, warehouseType2,"Almacen 2");
        List<Warehouse> expectedWarehouses = new ArrayList<>();
        expectedWarehouses.add(warehouse);
        expectedWarehouses.add(warehouse2);
        int expectedStatus = 200;

        String uri = "/v1/warehouses";

        when(warehouseService.getWarehouses()).thenReturn(expectedWarehouses);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        List<Warehouse> actualWarehouses = mapper.readValue(content,
                new TypeReference<List<Warehouse>>(){});

        assertEquals(expectedStatus, actualStatus);
        assertEquals(expectedWarehouses.size(), actualWarehouses.size());
        assertEquals(expectedWarehouses, actualWarehouses);
    }

    @Test
    void getWarehousesErrorTest() throws Exception{

        int expectedStatus = 500;

        String uri = "/v1/warehouses";

        when(warehouseService.getWarehouses()).thenThrow(new NullPointerException("Error"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);
    }

    @Test
    void createWarehouseTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        WarehouseType warehouseType1 = new WarehouseType(1, "publico");
        WarehouseDTO warehouseDTO = new WarehouseDTO();
        warehouseDTO.setName("Almacen 1");

        Warehouse expectedWarehouse = new Warehouse(1,warehouseType1, "Almacen 1");
        int expectedStatus = 201;

        String uri = "/v1/warehouses";

        String bodyString = mapper.writeValueAsString(warehouseDTO);

        when(warehouseService.createWarehouse(any(WarehouseDTO.class))).thenReturn(expectedWarehouse);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Warehouse actualWarehouse = mapper.readValue(content, Warehouse.class);

        assertEquals(expectedStatus, actualStatus);
        assertEquals(actualWarehouse.getName(), expectedWarehouse.getName());
    }

    @Test
    void createWarehouseErrorTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        WarehouseDTO warehouseDTO = new WarehouseDTO();
        warehouseDTO.setName("Almacen 1");

        int expectedStatus = 500;

        String uri = "/v1/warehouses";

        String bodyString = mapper.writeValueAsString(warehouseDTO);

        when(warehouseService.createWarehouse(any(WarehouseDTO.class))).thenThrow(new NullPointerException("Error"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);
    }
}

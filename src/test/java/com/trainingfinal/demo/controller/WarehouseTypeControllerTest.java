package com.trainingfinal.demo.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trainingfinal.demo.dto.ProductTypeDTO;
import com.trainingfinal.demo.dto.WarehouseTypeDTO;
import com.trainingfinal.demo.model.ProductType;
import com.trainingfinal.demo.model.WarehouseType;
import com.trainingfinal.demo.service.ProductTypeService;
import com.trainingfinal.demo.service.WarehouseTypeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(WarehouseTypeController.class)
public class WarehouseTypeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WarehouseTypeService warehouseTypeService;

    @Test
    void getWarehouseTypesTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        WarehouseType warehouseType = new WarehouseType(1, "publico");
        WarehouseType warehouseType2 = new WarehouseType(1, "zapatillas");
        List<WarehouseType> expectedWarehouseTypes = new ArrayList<>();
        expectedWarehouseTypes.add(warehouseType);
        expectedWarehouseTypes.add(warehouseType2);
        int expectedStatus = 200;

        String uri = "/v1/warehouseTypes";

        when(warehouseTypeService.getWarehouseTypes()).thenReturn(expectedWarehouseTypes);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        List<WarehouseType> actualWarehouseTypes = mapper.readValue(content,
                new TypeReference<List<WarehouseType>>(){});

        assertEquals(expectedStatus, actualStatus);
        assertEquals(expectedWarehouseTypes.size(), actualWarehouseTypes.size());
        assertEquals(expectedWarehouseTypes, actualWarehouseTypes);
    }

    @Test
    void getWarehouseTypesErrorTest() throws Exception{

        int expectedStatus = 500;

        String uri = "/v1/warehouseTypes";

        when(warehouseTypeService.getWarehouseTypes()).thenThrow(new NullPointerException("Error"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);
    }

    @Test
    void createWarehouseTypeTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        WarehouseTypeDTO warehouseTypeDTO = new WarehouseTypeDTO();
        warehouseTypeDTO.setName("publico");

        WarehouseType expectedWarehouseType = new WarehouseType(1, "publico");
        int expectedStatus = 201;

        String uri = "/v1/warehouseTypes";

        String bodyString = mapper.writeValueAsString(warehouseTypeDTO);

        when(warehouseTypeService.createWarehouseType(any(WarehouseTypeDTO.class))).thenReturn(expectedWarehouseType);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        WarehouseType actualWarehouseType = mapper.readValue(content, WarehouseType.class);

        assertEquals(expectedStatus, actualStatus);
        assertEquals(actualWarehouseType.getName(), expectedWarehouseType.getName());
    }

    @Test
    void createWarehouseTypeErrorTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        WarehouseTypeDTO warehouseTypeDTO = new WarehouseTypeDTO();
        warehouseTypeDTO.setName("publico");

        int expectedStatus = 500;

        String uri = "/v1/warehouseTypes";

        String bodyString = mapper.writeValueAsString(warehouseTypeDTO);

        when(warehouseTypeService.createWarehouseType(any(WarehouseTypeDTO.class))).thenThrow(new NullPointerException("Error"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);
    }
}

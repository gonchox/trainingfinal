package com.trainingfinal.demo.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trainingfinal.demo.dto.ProductTypeDTO;
import com.trainingfinal.demo.model.ProductType;
import com.trainingfinal.demo.service.ProductTypeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(ProductTypeController.class)
public class ProductTypeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductTypeService productTypeService;

    @Test
    void getProductTypesTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        ProductType productType = new ProductType(1, "ropa");
        ProductType productType2 = new ProductType(1, "zapatillas");
        List<ProductType> expectedProductTypes = new ArrayList<>();
        expectedProductTypes.add(productType);
        expectedProductTypes.add(productType2);
        int expectedStatus = 200;

        String uri = "/v1/productTypes";

        when(productTypeService.getProductTypes()).thenReturn(expectedProductTypes);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        List<ProductType> actualProductTypes = mapper.readValue(content,
                new TypeReference<List<ProductType>>(){});

        assertEquals(expectedStatus, actualStatus);
        assertEquals(expectedProductTypes.size(), actualProductTypes.size());
        assertEquals(expectedProductTypes, actualProductTypes);
    }

    @Test
    void getProductTypesErrorTest() throws Exception{

        int expectedStatus = 500;

        String uri = "/v1/productTypes";

        when(productTypeService.getProductTypes()).thenThrow(new NullPointerException("Error"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);
    }

    @Test
    void createProductTypeTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        ProductTypeDTO productTypeDTO = new ProductTypeDTO();
        productTypeDTO.setName("ropa");

        ProductType expectedProductType = new ProductType(1, "ropa");
        int expectedStatus = 201;

        String uri = "/v1/productTypes";

        String bodyString = mapper.writeValueAsString(productTypeDTO);

        when(productTypeService.createProductType(any(ProductTypeDTO.class))).thenReturn(expectedProductType);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        ProductType actualProductType = mapper.readValue(content, ProductType.class);

        assertEquals(expectedStatus, actualStatus);
        assertEquals(actualProductType.getName(), expectedProductType.getName());
    }

    @Test
    void createProductTypeErrorTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        ProductTypeDTO productTypeDTO = new ProductTypeDTO();
        productTypeDTO.setName("ropa");

        int expectedStatus = 500;

        String uri = "/v1/productTypes";

        String bodyString = mapper.writeValueAsString(productTypeDTO);

        when(productTypeService.createProductType(any(ProductTypeDTO.class))).thenThrow(new NullPointerException("Error"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);
    }

}

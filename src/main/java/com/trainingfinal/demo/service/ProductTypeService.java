package com.trainingfinal.demo.service;

import com.trainingfinal.demo.dto.ProductTypeDTO;
import com.trainingfinal.demo.model.ProductType;

import java.util.List;

public interface ProductTypeService {
    public List<ProductType> getProductTypes();
    public ProductType createProductType(ProductTypeDTO productTypeDto);
    public ProductType getProductTypeById(int id);

}

package com.trainingfinal.demo.service;

import com.trainingfinal.demo.dto.ProductTypeDTO;
import com.trainingfinal.demo.dto.WarehouseDTO;
import com.trainingfinal.demo.model.ProductType;
import com.trainingfinal.demo.model.Warehouse;

import java.util.List;

public interface WarehouseService {
    public List<Warehouse> getWarehouses();
    public Warehouse createWarehouse(WarehouseDTO warehouseDTO);
    public Warehouse getWarehouseById(int id);
}

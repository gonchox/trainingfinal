package com.trainingfinal.demo.service;

import com.trainingfinal.demo.dto.ProductDTO;
import com.trainingfinal.demo.model.Product;

import java.util.List;

public interface ProductService {

    public List<Product> getProducts();
    public Product createProduct(ProductDTO productDTO);
    public Product getProductById(int id);
}

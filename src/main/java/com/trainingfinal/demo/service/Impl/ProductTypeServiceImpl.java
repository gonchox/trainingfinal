package com.trainingfinal.demo.service.Impl;

import com.trainingfinal.demo.dto.ProductTypeDTO;
import com.trainingfinal.demo.model.ProductType;
import com.trainingfinal.demo.repository.ProductTypeRepository;
import com.trainingfinal.demo.service.ProductTypeService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class ProductTypeServiceImpl implements ProductTypeService {


    @Autowired
    private ProductTypeRepository productTypeRepository;

    @Override
    public List<ProductType> getProductTypes() {
        return productTypeRepository.getProductTypes();
    }

    @Override
    public ProductType createProductType(ProductTypeDTO productTypeDto) {
        ProductType productType = new ProductType(productTypeDto);
        return productTypeRepository.createProductType(productType);

    }

    @Override
    public ProductType getProductTypeById(int id) {
        return productTypeRepository.getProductTypeById(id);
    }
}

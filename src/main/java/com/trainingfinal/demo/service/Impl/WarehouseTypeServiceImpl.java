package com.trainingfinal.demo.service.Impl;

import com.trainingfinal.demo.dto.WarehouseTypeDTO;
import com.trainingfinal.demo.model.ProductType;
import com.trainingfinal.demo.model.WarehouseType;
import com.trainingfinal.demo.repository.WarehouseRepository;
import com.trainingfinal.demo.repository.WarehouseTypeRepository;
import com.trainingfinal.demo.service.WarehouseTypeService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class WarehouseTypeServiceImpl implements WarehouseTypeService {

    @Autowired
    private WarehouseTypeRepository warehouseTypeRepository;

    @Override
    public List<WarehouseType> getWarehouseTypes() {
        return warehouseTypeRepository.getWarehouseTypes();
    }

    @Override
    public WarehouseType createWarehouseType(WarehouseTypeDTO warehouseTypeDTO) {
        WarehouseType warehouseType = new WarehouseType(warehouseTypeDTO);
        return warehouseTypeRepository.createWarehouseType(warehouseType);
    }

    @Override
    public WarehouseType getWarehouseTypeById(int id) {
        return warehouseTypeRepository.getWarehouseTypeById(id);
    }
}

package com.trainingfinal.demo.service.Impl;

import com.trainingfinal.demo.dto.ProductDTO;
import com.trainingfinal.demo.dto.WarehouseDTO;
import com.trainingfinal.demo.dto.WarehouseXProductDTO;
import com.trainingfinal.demo.model.Product;
import com.trainingfinal.demo.model.Warehouse;
import com.trainingfinal.demo.model.WarehouseType;
import com.trainingfinal.demo.model.WarehouseXProduct;
import com.trainingfinal.demo.repository.ProductRepository;
import com.trainingfinal.demo.repository.ProductTypeRepository;
import com.trainingfinal.demo.repository.WarehouseRepository;
import com.trainingfinal.demo.repository.WarehouseXProductRepository;
import com.trainingfinal.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private WarehouseXProductRepository warehouseXProductRepository;
    @Autowired
    private WarehouseRepository warehouseRepository;

    @Override
    public List<Product> getProducts() {
        return productRepository.getProducts();

    }

    @Override
    @Transactional
    public Product createProduct(ProductDTO productDTO) {
        Product product = new Product(productDTO);
        product = productRepository.createProduct(product);

        for (WarehouseXProductDTO warehouseXProductDTO : productDTO.getWarehouseXProductDTOS()) {
            WarehouseXProduct warehouseXProduct = new WarehouseXProduct();
            warehouseXProduct.setProduct(product);
            warehouseXProduct.setWarehouse(warehouseRepository.getWarehouseById(warehouseXProductDTO.getIdWarehouse()));
            warehouseXProduct.setStock(warehouseXProductDTO.getStock());
            warehouseXProductRepository.createWarehouseXProduct(product.getId(), warehouseXProduct);
        }

        return product;
    }

    @Override
    public Product getProductById(int id) {
        return productRepository.getProductById(id);
    }
}

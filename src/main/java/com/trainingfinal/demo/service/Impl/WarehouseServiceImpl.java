package com.trainingfinal.demo.service.Impl;

import com.trainingfinal.demo.dto.WarehouseDTO;
import com.trainingfinal.demo.model.Product;
import com.trainingfinal.demo.model.ProductType;
import com.trainingfinal.demo.model.Warehouse;
import com.trainingfinal.demo.model.WarehouseType;
import com.trainingfinal.demo.repository.ProductRepository;
import com.trainingfinal.demo.repository.WarehouseRepository;
import com.trainingfinal.demo.repository.WarehouseTypeRepository;
import com.trainingfinal.demo.service.WarehouseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class WarehouseServiceImpl implements WarehouseService {

    @Autowired
    private WarehouseRepository warehouseRepository;
    @Autowired
    private WarehouseTypeRepository warehouseTypeRepository;

    @Override
    public List<Warehouse> getWarehouses() {
        List<Warehouse> warehouses = warehouseRepository.getWarehouses();
        return  warehouses;

    }

    @Override
    public Warehouse createWarehouse(WarehouseDTO warehouseDTO) {
        Warehouse warehouse = new Warehouse(warehouseDTO);

        WarehouseType warehouseType = warehouseTypeRepository.getWarehouseTypeById(warehouseDTO.getIdWarehouseType());
        warehouse.setWarehouseType(warehouseType);

        return warehouseRepository.createWarehouse(warehouse);
    }

    @Override
    public Warehouse getWarehouseById(int id) {
        return warehouseRepository.getWarehouseById(id);
    }
}

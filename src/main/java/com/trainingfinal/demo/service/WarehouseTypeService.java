package com.trainingfinal.demo.service;

import com.trainingfinal.demo.dto.ProductTypeDTO;
import com.trainingfinal.demo.dto.WarehouseTypeDTO;
import com.trainingfinal.demo.model.ProductType;
import com.trainingfinal.demo.model.WarehouseType;

import java.util.List;

public interface WarehouseTypeService {
    public List<WarehouseType> getWarehouseTypes();
    public WarehouseType createWarehouseType(WarehouseTypeDTO warehouseTypeDTO);
    public WarehouseType getWarehouseTypeById(int id);
}

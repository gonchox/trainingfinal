package com.trainingfinal.demo.model;
import com.trainingfinal.demo.dto.ProductTypeDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductType {

    private int id;
    private String name;

    public ProductType(ProductTypeDTO productTypeDTO){
        this.name= productTypeDTO.getName();
    }
}

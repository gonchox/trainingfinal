package com.trainingfinal.demo.model;

import com.trainingfinal.demo.dto.ProductDTO;
import com.trainingfinal.demo.dto.WarehouseXProductDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    private int id;
    private ProductType productType;
    private String name;
    private String sku;
    private String partNumber;
    private double cost;
    private int totalStock;
    private List<WarehouseXProduct> warehouseXProductsS;


    public Product(ProductDTO productDTO) {
        ProductType productType = new ProductType();
        productType.setId(productDTO.getIdProductType());
        this.productType = productType;
        this.name = productDTO.getName();
        this.sku=productDTO.getSku();
        this.partNumber=productDTO.getPartNumber();
        this.cost=productDTO.getCost();
    }

    public Product(Product productDTO) {

    }
}

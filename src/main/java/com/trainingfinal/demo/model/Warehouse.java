package com.trainingfinal.demo.model;

import com.trainingfinal.demo.dto.WarehouseDTO;
import com.trainingfinal.demo.dto.WarehouseTypeDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Warehouse {
    private int id;
    private WarehouseType warehouseType;
    private String name;

    public Warehouse(WarehouseDTO warehouseDTO){
        WarehouseType warehouseType = new WarehouseType();
        this.warehouseType = warehouseType;
        this.name=warehouseDTO.getName();
    }
}

package com.trainingfinal.demo.model;

import com.trainingfinal.demo.dto.WarehouseXProductDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WarehouseXProduct {
    private int id;
    private Warehouse warehouse;
    private Product product;
    private int stock;

    public WarehouseXProduct(WarehouseXProductDTO warehouseXProductDTO, Warehouse warehouse, Product product){
        this.warehouse = warehouse;
        this.product=product;
        this.stock=warehouseXProductDTO.getStock();
    }
}

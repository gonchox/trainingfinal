package com.trainingfinal.demo.repository;

import com.trainingfinal.demo.model.WarehouseXProduct;

import java.util.List;

public interface WarehouseXProductRepository {
    public List<WarehouseXProduct> getWarehouseXProductBySaleId(int productId);
    public WarehouseXProduct createWarehouseXProduct(int productId, WarehouseXProduct warehouseXProduct);

}

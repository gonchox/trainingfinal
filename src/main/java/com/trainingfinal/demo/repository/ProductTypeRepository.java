package com.trainingfinal.demo.repository;

import com.trainingfinal.demo.model.ProductType;

import java.util.List;

public interface ProductTypeRepository {
    public List<ProductType> getProductTypes();
    public ProductType createProductType(ProductType productType);
    public ProductType getProductTypeById(int id);

}

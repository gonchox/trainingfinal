package com.trainingfinal.demo.repository;

import com.trainingfinal.demo.model.WarehouseType;

import java.util.List;

public interface WarehouseTypeRepository {
    public List<WarehouseType> getWarehouseTypes();
    public WarehouseType createWarehouseType(WarehouseType warehouseType);
    public WarehouseType getWarehouseTypeById(int id);
}

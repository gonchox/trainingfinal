package com.trainingfinal.demo.repository;

import com.trainingfinal.demo.model.Product;

import java.util.List;

public interface ProductRepository {
    public List<Product> getProducts();
    public Product createProduct(Product product);
    public Product getProductById(int id);

}

package com.trainingfinal.demo.repository.Impl;

import com.trainingfinal.demo.model.Warehouse;
import com.trainingfinal.demo.model.WarehouseXProduct;
import com.trainingfinal.demo.repository.WarehouseXProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class WarehouseXProductRepositoryImpl implements WarehouseXProductRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<WarehouseXProduct> getWarehouseXProductBySaleId(int productId) {
        String sql = "SELECT * FROM WarehouseXProduct WHERE idProduct = ?";
        List<WarehouseXProduct> warehouseXProductList = jdbcTemplate.query(sql,
                (rs, rowNum) -> mapToWarehouseXProduct(rs), productId);
        return warehouseXProductList;

    }

    private WarehouseXProduct mapToWarehouseXProduct(ResultSet rs) throws SQLException {
       WarehouseXProduct warehouseXProduct = new WarehouseXProduct();
        warehouseXProduct.setId(rs.getInt("id"));
        Warehouse warehouse = new Warehouse();
        warehouse.setId(rs.getInt("idWarehouse"));
        warehouseXProduct.setStock(rs.getInt("stock"));

        warehouseXProduct.setWarehouse(warehouse);

        return warehouseXProduct;
    }


    @Override
    public WarehouseXProduct createWarehouseXProduct(int productId, WarehouseXProduct warehouseXProduct) {
        SimpleJdbcInsert simpleJdbcInsert =
                new SimpleJdbcInsert(jdbcTemplate.getDataSource())
                        .withTableName("WarehouseXProduct")
                        .usingGeneratedKeyColumns("id");

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idProduct", productId);
        parameters.put("idWarehouse", warehouseXProduct.getWarehouse().getId());
        parameters.put("stock", warehouseXProduct.getStock());
        int id = simpleJdbcInsert.executeAndReturnKey(parameters).intValue();
        warehouseXProduct.setId(id);
        return warehouseXProduct;

    }
}

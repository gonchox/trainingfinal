package com.trainingfinal.demo.repository;

import com.trainingfinal.demo.model.Warehouse;

import java.util.List;

public interface WarehouseRepository {
    public List<Warehouse> getWarehouses();
    public Warehouse createWarehouse(Warehouse warehouse);
    public Warehouse getWarehouseById(int id);
}

package com.trainingfinal.demo.dto;


import lombok.Data;

import java.util.ArrayList;

@Data
public class ProductDTO {
    private int idProductType;
    private String name;
    private String sku;
    private String partNumber;
    private double cost;
    private int totalStock;
    private ArrayList<WarehouseXProductDTO> warehouseXProductDTOS;
}

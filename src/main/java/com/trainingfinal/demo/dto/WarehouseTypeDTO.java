package com.trainingfinal.demo.dto;

import lombok.Data;

@Data
public class WarehouseTypeDTO {
    private String name;
}

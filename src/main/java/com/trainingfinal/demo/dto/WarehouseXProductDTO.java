package com.trainingfinal.demo.dto;

import lombok.Data;

@Data
public class WarehouseXProductDTO {
    private int idWarehouse;
    private int stock;
}

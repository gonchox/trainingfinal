package com.trainingfinal.demo.dto;

import lombok.Data;

@Data
public class WarehouseDTO {
    private String name;
    private int idWarehouseType;
}

package com.trainingfinal.demo.dto;

import lombok.Data;

@Data
public class ProductTypeDTO {
    private String name;
}
